#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include <cstdint>
#include "Block.h"

class Blockchain {
public:
    Blockchain(Block *InitBlock);

    void AddBlock(Block *NewBlock);

private:
    uint32_t _nDifficulty;
    Block* _LastBlock;
    
    void _IncreaseDifficulty();
};

#endif // BLOCKCHAIN_H
