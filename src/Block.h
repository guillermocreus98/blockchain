#ifndef BLOCK_H
#define BLOCK_H

//#include <cstdint>
//#include <iostream>


#include <sstream>
#include "Transactions.h"

class Block {
public:
    string sHash; 
    Block* PrevBlock;
    
    Block(uint32_t nIndexIn, const Transactions &Ts);

    void MineBlock(uint32_t nDifficulty);

private:
    uint32_t _nIndex;
    uint32_t _nNonce;    
    time_t _timestamp;
    Transactions _Ts;
    
    string _CalculateHash() const; // Hash del nIndex PrevHash time Data Nonce
};

#endif // BLOCK_H
