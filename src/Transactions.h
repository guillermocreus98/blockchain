#ifndef TRANSACTIONS_H   
#define TRANSACTIONS_H

#include <vector>
#include "Transaction.h"

class Transactions {
public:
    Transactions();
    void AddTransaction(const Transaction& T);

private:
    vector<Transaction> T_s;
};

#endif  // TRANSACTIONS_H
