#include "Blockchain.h"

Blockchain::Blockchain(Block *InitBlock)  // InitBlock can be nullptr
{
  _LastBlock = InitBlock;
  _nDifficulty = 1;
}

void Blockchain::AddBlock(Block *NewBlock)  // Make as a linked list.
{
  NewBlock->PrevBlock = _LastBlock;  
  _LastBlock = NewBlock;
}

void Blockchain::_IncreaseDifficulty()  // Increase _nDifficulty by 1
{
  _nDifficulty++;
}
